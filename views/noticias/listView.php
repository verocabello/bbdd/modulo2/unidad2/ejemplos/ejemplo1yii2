<div>
    <h1>Listado de todos los registros</h1>
</div>
<?php
use yii\widgets\ListView;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
]);

